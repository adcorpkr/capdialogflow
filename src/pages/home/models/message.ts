export interface Message {
  from: 'bot' | 'user';
  text: string;
  matchFlag: boolean;
}
