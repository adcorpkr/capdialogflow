import { Component, ViewChild } from '@angular/core';
import { Platform, Content } from 'ionic-angular';
import { ApiAiClient } from 'api-ai-javascript';
import { Message } from './models/message';
import { FormControl, FormBuilder } from '@angular/forms';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild(Content) content: Content;
  accessToken: string = '24939631df174e08a5914faf48adf407';
  client;
  messages: Message[] = [];
  timestamp: any;
  messageForm: any;
  chatBox: any;
  isLoading: boolean;


  matchFlag: boolean = false;
  uniforms = [
    { id: '01', imgPath: "img/black_l.png", color: "Black"},
    { id: '02', imgPath: "img/blue_l.png", color: "Blue"},
    { id: '03', imgPath: "img/darkblue_l.png", color: "Darkblue"},
    { id: '04', imgPath: "img/green_l.png", color: "Green"},
    { id: '05', imgPath: "img/orange_l.png", color: "Orange"},
    { id: '06', imgPath: "img/red_l.png", color: "Red"}
  ]

  constructor(public platform: Platform, public formBuilder: FormBuilder) {
    this.chatBox = '';

    this.messageForm = formBuilder.group({
      message: new FormControl('')
    });

    this.client = new ApiAiClient({
      accessToken: this.accessToken
    });
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad HomePage');
    this.messages.push({
      from: 'bot',
      text: '안녕하세요? Mr.WANG입니다. 문의사항이 있으시다면 아래 목록에서 선택해주시거나 원하시는 질문을 채팅창에 직접 입력해주세요.',
      matchFlag: false
    });
    this.messages.push({
      from: 'bot',
      text: '채팅으로 대화를 하실 때에는 한 질문은 한 번에 말씀해주시면 감사하겠습니다! 여러 번으로 나눠 말씀하시면 제가 이해할 수가 없어요 ㅠ_ㅠ',
      matchFlag: true
    });
  }

  pushImageMemu() {
    this.messages.push({
      from: 'bot',
      text: '다른 Color를 선택해 주시기 바랍니다.',
      matchFlag: true
    });
  }

  sendMessage(req: string) {
    if (!req || req === '') {
      return;
    }
    let matchFlag: boolean = false;
    this.messages.push({ from: 'user', text: req , matchFlag: false});
    this.isLoading = true;

    this.client
      .textRequest(req)
      .then(response => {
        /* do something */
        console.log('res');
        console.log(response);
        console.log("Request ===> "+req);
        
        let arr = [];
        arr = response.result.fulfillment.speech;
        let str = arr.toString(); 
        console.log("Returned string is : " + str );

        if (str.indexOf('죄송합니다') != -1) {
          matchFlag = true;
          console.log("matchFlag true ===> "+matchFlag);
        } else {
          matchFlag = false;
          console.log("matchFlag false ===> "+matchFlag);
        }

        this.messages.push({
          from: 'bot',
          text: response.result.fulfillment.speech,
          matchFlag: matchFlag
        });
        this.timestamp = response.timestamp;
        this.scrollToBottom();
        this.isLoading = false;
      })
      .catch(error => {
        /* do something here too */
        console.log('error');
        console.log(error);
      });
      
    this.chatBox = '';
  }

  selectedUniform(u){
    console.log("selectedUniform: "+u.color);
    this.sendMessage(u.color)
  }

  scrollToBottom() {
    setTimeout(() => {
      this.content.scrollToBottom();
    }, 100);
  }
}
